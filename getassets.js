var unirest = require('unirest');
var mkrequest = require('./mkrequest')

function getAssets() {
    unirest.post(mkrequest('/asset/find'))

    .headers(

        { 'Content-Type': 'application/json', 'Accept' : 'application/json' } )

    .send(

        {'serialNumber': 'pbu*' } )

    .end(function (response) {
      	for (i=0; i<response.body.totalCount; i++) {
        	console.log(
        	    response.body.assets[i].serialNumber + '    ' +
        	    response.body.assets[i].systemId
        	    )
    	}
    })
};


getAssets()
