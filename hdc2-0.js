var unirest = require('unirest');
var mod = require('./mk2request')
var mk2 = mod.mk2request;
var svc = mod.svc;

function getDevices() {
    unirest.get(mk2(svc.sm + '/devices'))

    .headers(

        { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json',
        }
    )

    .send()

    .end(function (response) {
      	//console.log(response);
        
        for (i=response.body.data.startIndex;
      		 i<response.body.data.startIndex +
      		   response.body.data.currentItemCount; i++) {
        	console.log(
        	    response.body.data.items[i].id + '    ' +
        	    response.body.data.items[i].uuid + '	' +
        	    response.body.data.items[i].properties.hostname
        	    )
    	}
    })
};

// 'Authorization': 
// 'Basic cGF1bC5idXRsZXJAd2luZHJpdmVyLmNvbTpQYXVsQFdpbmQxMjM0'

getDevices()
