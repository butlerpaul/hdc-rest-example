var unirest = require('unirest');
var mod = require('./mk2request')
var mk2 = mod.mk2request;
var svc = mod.svc;

function getDevices() {
    unirest.get(mk2(svc.tm + '/datasources?$filter={deviceId:["*"]}'))

    .headers(

        { 'Content-Type': 'application/json', 'Accept' : 'application/json' } )

    .send()

    .end(function(response) {
        console.log(response.body)
    })

};


getDevices()
