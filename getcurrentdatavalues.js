var unirest = require('unirest');
var mkrequest = require('./mkrequest')

function getCurrentDataValues(asset) {
    unirest.post(mkrequest('/dataItem/findCurrentValues'))

    .headers(

        { 'Content-Type': 'application/json', 'Accept' : 'application/json' } )

    .send(

        { "assetId" : asset } )

    .end(function (response) {
      	for (i=0; i<response.body.totalCount; i++) {
        	console.log(
        		response.body.dataItemValues[i].dataItem.name + '   ' +
        		response.body.dataItemValues[i].timestamp + ' ' +
        		response.body.dataItemValues[i].value
        		)
    	}
    })
};

getCurrentDataValues(process.argv[2])
